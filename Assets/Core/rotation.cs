﻿using UnityEngine;
using System.Collections;

public class rotation : MonoBehaviour
{
    /// <summary>
    /// There are some issues with this part, we'll come back for it later.
    /// The better idea is to create a Sphere, that the camera is going to track
    /// The camera always looks at the center of the Sphere and when we use arrows or mouse to change the point
    ///     view, we move the sphere.
    /// 
    /// </summary>
    public float minX = -360;
    public float maxX = 360;

    public float minY = -45;
    public float maxY = 45;

    public float xSpeed = 100;
    public float ySpeed = 100;

    float rotationY = 0;
    float rotationX = 0;

    private float zoomSpeed = 2;

    public float movement_speed = 50;
    //private Camera cam;

    // Use this for initialization
    void Start ()
    {
        //cam = GetComponent<Camera>();
    }

    void Update()
    {
        RotateCamera();
        MoveCamera();
    }

    void MoveCamera()
    {
        float y = transform.position.y;
        float horizontal = Input.GetAxis("Horizontal") * movement_speed * Time.deltaTime;
        float vertical = Input.GetAxis("Vertical") * movement_speed * Time.deltaTime;
        transform.Translate(Vector3.forward * vertical);
        transform.Translate(Vector3.right * horizontal);
        transform.position = new Vector3(transform.position.x, y, transform.position.z);
    }

    void RotateCamera()
    {
        if (Input.GetMouseButton(2))
        {
            rotationX += Input.GetAxis("Mouse X") * xSpeed * Time.deltaTime;
            rotationY += Input.GetAxis("Mouse Y") * ySpeed * Time.deltaTime;
            transform.eulerAngles = new Vector3(-rotationY, rotationX, 0);
        }
    }

    void ZoomCamera()
    {

    }
}
