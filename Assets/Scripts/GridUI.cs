﻿using UnityEngine;
using System.Collections;

public class GridUI : MonoBehaviour
{
    private Command control;
    private Camera mainCamera;

	// Use this for initialization
	void Start ()
    {
        control = GameObject.Find("UI").GetComponent<Command>(); // getting the center of the UI system for this game
        mainCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
	}

    void OnMouseOver()
    {
        if (Input.GetMouseButton(0))
        {
            ProduceRaycast(0);
        }
        if (Input.GetMouseButton(1))
        {
            ProduceRaycast(1);
        }
    }

    private void ProduceRaycast(int button)
    {
        RaycastHit hit;
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        Physics.Raycast(ray, out hit); // hit has to be on the surface of the grid!
        control.HandleMouseClick(hit, button); // especialy on this function
    }
}
