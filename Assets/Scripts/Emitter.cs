﻿using UnityEngine;
using System.Collections;

public class Emitter : MonoBehaviour
{
    public uint rate; // the rate of particles to spawn
    private ParticleSystem particle_system; // the particle system object
    private ParticleSystem.EmissionModule emission; // the emmision module, to control rate and switch state

    // Use this for initialization
    void Start ()
    {
        particle_system = GetComponent<ParticleSystem>();
        ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve(rate);
        emission = particle_system.emission;
        emission.rate = curve;
    }

    public void Finish()
    {
        // setting the destruction properly
        // smoke:
        emission.rate = new ParticleSystem.MinMaxCurve(0);
        

        // halo effect:
        Behaviour h = (Behaviour)(transform.Find("EngineFire").GetComponent("Halo"));
        h.enabled = false;

        // fire:
        if (GetComponentsInChildren<ParticleSystem>().GetLength(0) >= 2)
        {
            Destroy(GetComponentsInChildren<ParticleSystem>()[1]);
        }
    }

    public void Activate()
    {
        emission.enabled = true;
    }
    public void Disable()
    {
        emission.enabled = false;
    }

    public bool IsActive
    {
        get
        {
            return emission.enabled;
        }
    }
}
