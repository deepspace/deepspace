﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class ifPlayerLeave : MonoBehaviour {

    private Rect errorRect = new Rect(0, 0, 300, 350);
    private string problemsData = "";
    private bool wantedToExit = false;

    // Use this for initialization
    void Start ()
    {
        if(!Network.isClient && !Network.isServer)
        {
            enableAlmostAllObjects();
            problemsData = "player disconnected";
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}


    private void OnGUI()
    {
        if (problemsData != "" && !wantedToExit) // if there was a problem in the connection.
        {
            errorRect = GUI.Window(0, errorRect, error_func, "Oh-no!");
        }
    }

    void OnApplicationQuit()
    {
        if (Network.isClient)
            Network.Disconnect();
        wantedToExit = true;
    }

    void OnDisconnectedFromServer(NetworkDisconnection info)
    {
        enableAlmostAllObjects();
        problemsData = "player disconnected";
    }

    void OnPlayerDisconnected(NetworkPlayer player)
    {
        enableAlmostAllObjects();
        problemsData = "player disconnected";
    }

    private void error_func(int id)
    {
        GUILayout.Box(problemsData);
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Go Back"))
            SceneManager.LoadScene("ChoseEnemy");
        GUILayout.EndHorizontal();
        GUI.DragWindow(new Rect(0, 0, Screen.width, Screen.height));
    }

    void enableAlmostAllObjects()
    {
        List<GameObject> allObjects = new List<GameObject>( FindObjectsOfType<GameObject>());
        for (int i = 0; i < allObjects.Count; i++)
        {
            if (allObjects[i].tag != "MainCamera")
                allObjects[i].SetActive(false);
        }
    }
}
