﻿using UnityEngine;
using System.Collections;

public class RotateStarSphere : MonoBehaviour
{
    public float rotationSpeed;
    private float x;
    private float y;
    private float z;

	void Start ()
    {
        x = Random.Range(-rotationSpeed, rotationSpeed) * rotationSpeed;
        y = Random.Range(-rotationSpeed, rotationSpeed) * rotationSpeed;
        z = Random.Range(-rotationSpeed, rotationSpeed) * rotationSpeed;
    }
	void Update ()
    {
        transform.Rotate(x, y, z);
    }
}
