﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour
{
    /// <summary>
    /// There are some issues with this part, we'll come back for it later.
    /// The better idea is to create a Sphere, that the camera is going to track
    /// The camera always looks at the center of the Sphere and when we use arrows or mouse to change the point
    ///     view, we move the sphere.
    /// 
    /// </summary>
    public float minX = -360;
    public float maxX = 360;

    public float minY = -45;
    public float maxY = 45;

    public float xSpeed = 100;
    public float ySpeed = 100;

    public float moveSpeed = 10f;

    private float rotationY = 0;
    private float rotationX = 0;

    private float zoomSpeed = 2;

    // Use this for initialization
    void Start ()
    {
        
    }

    void Update()
    {
        RotateCamera();
        MoveCamera();
    }

    private void MoveCamera()
    {
        float res;
        float y = transform.position.y;
        float horizontal = Input.GetAxis("Horizontal") * moveSpeed;
        float vertical = Input.GetAxis("Vertical") * moveSpeed;
        res = Mathf.Sqrt(Mathf.Pow(horizontal, 2) + Mathf.Pow(vertical, 2)); // making the move of the camera smooth
        transform.Translate((Vector3.forward * vertical + Vector3.right * horizontal).normalized * res); // the output vector(1,y,z) * 'smoothing'-velocity
        transform.position = new Vector3(transform.position.x, y, transform.position.z); // fixing y-offset
    }

    private void RotateCamera()
    {
        if (Input.GetMouseButton(2))
        {
            rotationX += (Input.GetAxis("Mouse X") * xSpeed * Time.deltaTime) % 360;
            rotationY += (Input.GetAxis("Mouse Y") * ySpeed * Time.deltaTime) % 360;
        }
        transform.eulerAngles = new Vector3(-rotationY, rotationX, 0);
    }

    //
    private void ZoomCamera()
    {

    }
}
