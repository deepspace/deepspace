﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Command : MonoBehaviour
{
    private GameObject currentlySelected;
    private GameObject multipleSelected; // unfinished feature
    public float altitude;
    public Color myColor;
    public Color opponentColor;
    public GameObject myWarp;

    public GameObject grid;

    public List<GameObject> spaceships;

	// Use this for initialization
	void Start ()
    {
        spaceships = new List<GameObject>();
        grid = GameObject.Find("Grid");
        myWarp.GetComponent<ParticleSystem>().startColor = myColor;
        myWarp.GetComponentInChildren<LensFlare>().color = myColor;
    }

    /// <summary>
    /// event handler sub-function - connects between player oreders and game object
    /// </summary>
    /// <param name="obj">The selected game object</param>
    public void SelectObject(GameObject obj)
    {
        this.currentlySelected = obj;
    }

    // old mouse selection and ordering system
    public void HandleMouseClick(RaycastHit hit, int button)
    {
        if (button == 1)
        {
            int count = 0;
            Vector3 target = hit.point;
            target.y = altitude;
            for (int i = 0; i < spaceships.Count; i++)
            {
                Spaceship script = spaceships[i].GetComponent<Spaceship>();
                if (script.IsSelected)
                {
                    script.SetDestination(target);
                    target.x += 100;
                    target.y += 100;
                }
            }
        }
    }

    public GameObject CurrentlySelected
    {
        get
        {
            return currentlySelected;
        }
    }

}
