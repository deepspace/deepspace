﻿using UnityEngine;
using System.Collections;

public class ConstructionTab : MonoBehaviour
{
    private UISpaceship uiship;
    private GameObject button;
    private NetworkView net;
    private Command control;

    // this script should be used very carefully!
    // since it is almost the only one to be used with constructor insted of Start, which is not working ever!
    public ConstructionTab(UISpaceship ship, GameObject button)
    {
        this.uiship = ship;
        this.button = button;

        control = GameObject.Find("UI").GetComponent<Command>();
        //net = gameObject.AddComponent<NetworkView>();
        //net.stateSynchronization = NetworkStateSynchronization.ReliableDeltaCompressed;
    }

    public UISpaceship Ship
    {
        get
        {
            return this.uiship;
        }
    }
    public GameObject Button
    {
        get
        {
            return this.button;
        }
    }

    public void onClick()
    {
        CreateShip();
        //net.RPC("CrateShip", RPCMode.All, null);
    }

    public void CreateShip()
    {
        //GameObject newShip = (GameObject)Network.Instantiate(uiship.Prefab, control.myWarp.transform.position, control.myWarp.transform.rotation, 0);
        GameObject newShip = (GameObject)Instantiate(uiship.Prefab, control.myWarp.transform.position, control.myWarp.transform.rotation);
        Spaceship script = newShip.GetComponentInChildren<Spaceship>();
        script.SetDestination(control.myWarp.transform.Find("Spawn").transform.position);
        script.enabled = true; // making 100% sure that the script is activated!
        newShip.tag = "me"; // setting the ship to have the tag of the current player
        RegisterShip(newShip);
    }

    public void RegisterShip(GameObject ship)
    {
        control.spaceships.Add(ship);
    }
}
